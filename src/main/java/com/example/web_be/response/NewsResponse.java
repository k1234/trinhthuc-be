package com.example.web_be.response;

import com.example.web_be.Entity.Comment;
import com.example.web_be.Entity.Message;
import com.example.web_be.Entity.Tags;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewsResponse {
    private Message message;
    private List<Tags> tags;
    private List<Comment> comments;
}
