package com.example.web_be.Controller;

import com.example.web_be.Entity.Tags;
import com.example.web_be.Repository.MessageRepository;
import com.example.web_be.Repository.TagRepository;
import com.example.web_be.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TagController {
    @Autowired
    TagRepository tagRepository;

    @Autowired
    MessageRepository messageRepository;



    TagService tagService =new TagService();
    @PostMapping("tag/post")
    public ResponseEntity<Object> createTag(@RequestBody Tags tag){
        return ResponseEntity.ok(tagRepository.save(tag));
    }

    @GetMapping("tag/getAll")
    public ResponseEntity<Object> getAllTag() {
        return ResponseEntity.ok(tagRepository.findAll());
    }


    @PutMapping("tag/update/{id}")
    public ResponseEntity<Object> UpdateTag(@PathVariable Integer id,@RequestBody Tags tag){
        Tags newTag= tagRepository.findTagById(id);
        tagService.TagConverter(newTag,tag);
        return ResponseEntity.ok(tagRepository.save(newTag));
    }
}
