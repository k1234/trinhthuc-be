package com.example.web_be.Controller;

import com.example.web_be.Entity.Message;
import com.example.web_be.Entity.Message_Tag;
import com.example.web_be.Entity.Tags;
import com.example.web_be.Repository.MessageRepository;
import com.example.web_be.Repository.Mss_Tag_repo;
import com.example.web_be.Repository.TagRepository;
import com.example.web_be.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class MessageController {
    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    TagRepository tagRepository;

    @Autowired
    Mss_Tag_repo mss_tag_repo;

    private MessageService messageService=new MessageService();
    @PostMapping("message/post")
    public ResponseEntity<Object> createMessage(@RequestBody Message message){
        return ResponseEntity.ok(messageRepository.save(message));
    }
    @GetMapping("message/getAll")
    public ResponseEntity<Object> getAllMessage() {
        return ResponseEntity.ok(messageRepository.findAll());
    }

    @GetMapping("message/search")
    public ResponseEntity<?> getAllMessageByKeyWord(@RequestParam(value = "content")String content) {

        return ResponseEntity.ok(messageRepository.findAllByContent(content));
    }
    @PutMapping("message/update/{id}")
    public ResponseEntity<Object> UpdateMessage(@PathVariable Integer id,@RequestBody Message message){
        Message newMessage = messageRepository.findMessageById(id);
        messageService.converter(message,newMessage);
        return ResponseEntity.ok(messageRepository.save(newMessage));
    }

    @PostMapping("message/{messageId}/tag/{tagId}")
    public ResponseEntity<Object> addTagtoMessage(
            @PathVariable Integer messageId,
            @PathVariable Integer tagId
    ) {
        Message_Tag message_tag = new Message_Tag();
        Message message = messageRepository.findMessageById(messageId);
        Tags tag = tagRepository.findTagById(tagId);
        message_tag.setMessage(message);
        message_tag.setTag(tag);
        return ResponseEntity.ok(mss_tag_repo.save(message_tag));
    }

    @PutMapping("update/message/{messageId}/tag/{tagId}")
    public ResponseEntity<Object> updateTagtoMessage(
            @PathVariable Integer messageId,
            @PathVariable Integer tagId,
            @RequestParam  (name="newTagId") Integer newTagId
    ) {
        Message_Tag message_tag = mss_tag_repo.findToUpdate(messageId,tagId);
        message_tag.setTag(tagRepository.findTagById(newTagId));
        return ResponseEntity.ok(mss_tag_repo.save(message_tag));
    }



}
