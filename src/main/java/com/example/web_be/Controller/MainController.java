package com.example.web_be.Controller;

import com.example.web_be.Repository.CommentRepository;
import com.example.web_be.Repository.MessageRepository;
import com.example.web_be.Repository.Mss_Tag_repo;
import com.example.web_be.response.NewsResponse;
import com.example.web_be.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class MainController {
    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private Mss_Tag_repo mss_tag_repo;

    private CommentService commentService=new CommentService();


    @DeleteMapping("message/delete/{id}")
    public ResponseEntity<?> deleteMessage(@PathVariable Integer id) {
        for(Integer ids : commentRepository.deleteByMessage_id(id))
        {
            messageRepository.deleteById(ids);
        }

        return ResponseEntity.ok("Delete complete!");
    }

    @DeleteMapping("comment/delete")
    public ResponseEntity<?> deleteComment(@RequestParam (value="id")Integer id) {
        commentRepository.deleteById(id);
        return ResponseEntity.ok("Delete complete!");
    }


    @GetMapping("News/{id}")
    public ResponseEntity<?> getNews(@PathVariable Integer id) {
        NewsResponse newsResponse=new NewsResponse();
        newsResponse.setMessage(messageRepository.findMessageById(id));
        newsResponse.setTags(mss_tag_repo.findTagsbyId(id));
        newsResponse.setComments(commentRepository.findAllByMessage_id(id));
        return ResponseEntity.ok(newsResponse);
    }





}
