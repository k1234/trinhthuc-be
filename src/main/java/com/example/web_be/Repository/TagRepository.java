package com.example.web_be.Repository;

import com.example.web_be.Entity.Tags;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TagRepository extends JpaRepository<Tags,Integer> {
    @Query("select tags from Tags tags where tags.id=:id")
    Tags findTagById(Integer id);
}
