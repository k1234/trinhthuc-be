package com.example.web_be.Repository;

import com.example.web_be.Entity.Message_Tag;
import com.example.web_be.Entity.Tags;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface Mss_Tag_repo extends JpaRepository<Message_Tag,Integer> {
    @Query("select message_tag.tag from Message_Tag message_tag where message_tag.message.id=:id")
    List<Tags> findTagsbyId(Integer id);

    @Query("select message_tag from Message_Tag message_tag where (message_tag.tag.id=:tg_id and message_tag.message.id=:ms_id)")
    Message_Tag findToUpdate(Integer ms_id,Integer tg_id);



}
