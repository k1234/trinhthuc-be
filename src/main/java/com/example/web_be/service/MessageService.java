package com.example.web_be.service;

import com.example.web_be.Entity.Message;

public class MessageService {
    public Message converter(Message message,Message newMessage){
        newMessage.setContent(message.getContent());
        newMessage.setHeading(message.getHeading());
        newMessage.setCreateDate(message.getCreateDate());
        newMessage.setThumbnail(message.getThumbnail());
        newMessage.setShortDescription(message.getShortDescription());
        newMessage.setUpdateById(message.getUpdateById());
        newMessage.setUpdateDate(message.getUpdateDate());
        return newMessage;
    }

}
