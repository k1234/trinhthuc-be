package com.example.web_be.service;

import com.example.web_be.Entity.Comment;

public class CommentService {
    public Comment converter(Comment comment,Comment newComment){
        newComment.setComment(comment.getComment());
        newComment.setUpdateDate(comment.getUpdateDate());
        return newComment;
    }
}
