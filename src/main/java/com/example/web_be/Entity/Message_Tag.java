package com.example.web_be.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name="Message_Tag")
public class Message_Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name="message_id",nullable = false)
    private Message message;
    @ManyToOne
    @JoinColumn(name="Tag_id",nullable = false)
    private Tags tag;
}
