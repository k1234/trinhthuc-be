package com.example.web_be.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name="Message")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "createBy")
    private Integer createById;

    @Column(name = "updateBy")
    private  Integer updateById;

    @Column(name = "createDate")
    private Date createDate;

    @Column(name = "updateDate")
    private Date updateDate;

    @Column(name="heading",nullable = false,columnDefinition = "TEXT")
    private String heading;

    @Column(name="content",nullable = false,columnDefinition = "TEXT")
    private String content;

    @Lob
    @Column(name = "thumbnail")
    private String thumbnail;

    @Column(name = "shortdescription",columnDefinition = "TEXT")
    private String shortDescription;

    @Column
    private Integer views;

}
